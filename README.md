# Nonogram solving via SAT #

This repo implements a nonogram solver using `pyeda` and its included SAT solver (picoSAT).

Solving nonograms via SAT is inefficient, the Wikipedia page contains information on more
sophisticated strategies.

The nonogram solver implemented in this repo performs the following steps:

1. Read a nonogram specification from a JSON file (see _File Format_)

2. For every row and column, generate all possible combinations of colored blocks

3. From these combinations (again, for each row and column), generate boolean equations
   describing the possible colored blocks. This step uses a boolean variable for each
   square in the nonogram solution. `true` corresponds to colored, `false` corresponds to blank.
   Each row and each column is then described in DNF. At least one of the terms of this DNF
   must be true in a solution for the nonogram. The DNF terms are thus ANDed together.

4. SAT solvers typically require CNF terms. `pyeda` provides an implementation of Tseytin's transformation
   to convert an arbitrary boolean expression to CNF efficiently.

5. Solve the CNF using `pyeda`'s built-in SAT solver (picosat)

6. Print the solved nonogram to the terminal

## Prerequisites ##

* Python 3.7
* PyEDA


