import json
import copy
import logging
from pyeda.inter import *
import sys
import multiprocessing
import re
import argparse

def generate_possibilities(combinations,length):
    """ Generate all possibly combinations of colored blocks in a row or column
        Returns a list of possible masks that can then be converted into a boolean expression.
        E.g., 2 2 in a row of length 6 can accomodate the following combinations:
            A B C D E F
            X X . X X . => A  & B & ~C &  D & E & ~F
            X X . . X X => A  & B & ~C & ~D & E &  F
            . X X . X X => ~A & B & C  & ~D & E &  F 
    """

    masks = []

    def _generate_possibilities(combinations,mark,limit,mask,masks):
        """ Recursively fill out a mask with possible combinations of colored blocks """
        if not len(combinations):
            masks.append(mask)
            return
        elif limit >= len(mask):
            return
        for start_position in range(limit,len(mask)):
            m = copy.deepcopy(mask)
            for i in range(start_position,start_position+combinations[0]):
                if (i >= length):
                    return
                else:
                    m[i] = mark
            _generate_possibilities(combinations[1:],mark+1,start_position+combinations[0]+1,m,masks)

    mask = [0 for i in range(length)]
    _generate_possibilities(combinations,1,0,mask,masks)
    return masks

def sat(terms):
    """ Perform Tseitin's encoding on the boolean terms describing the Nonogram,
        then solve it using pyeda's SAT solver (picosat) """
    logging.info('Performing Tseitin encoding.')
    all_cnf = terms.tseitin()
    logging.info('Terms are now encoded. Solving SAT.')
    return all_cnf.satisfy_one()
    

def print_solution(solution,ncol,nrow):
    grid = [[' ' for col in range(ncol)] for row in range(nrow)]
    for grid_xy in solution:
        # Stringly-typed (:
        # parse back the variable name to get an X/Y coordinate
        m = re.match('x(?P<x>[0-9]+)y(?P<y>[0-9]+)',str(grid_xy))
        if not m:
            continue
        x = m.groupdict()['x']
        y = m.groupdict()['y']
        if solution[grid_xy]:
            grid[int(y)][int(x)] = '#'
    for row in grid:
        print(''.join(row))

def main(input_file):
    """ Read a JSON nonogram file, generate all possible combinations of colored blocks
        in each row and column, combine these terms and use a SAT solver to solve the 
        nonogram """

    logging.basicConfig(level=logging.INFO)
    with open(input_file,'r') as f:
        spec = json.load(f)
    cols = spec['cols']
    rows = spec['rows']
    logging.info('columns: {}, rows: {}'.format(len(cols),len(rows)))

    # Create symbols for every cell in the nonogram
    sym_names = ['x{}y{}'.format(x,y) for x in range(len(cols)) for y in range(len(rows))]
    sym_ixes  = dict(zip(sym_names,[1+x*len(cols)+y for x in range(len(cols)) for y in range(len(rows))]))
    syms = dict(zip(sym_names,[exprvar(sym_name) for sym_name in sym_names]))

    # Build terms for the columns
    logging.info('Building column terms')

    col_terms = []

    for colix,col in enumerate(cols):
        logging.info('col {}/{}'.format(colix+1,len(cols)))
        possibilities = generate_possibilities(col,len(rows))
        col_term = 0
        # convert every combination into boolean terms
        for px,p in enumerate(possibilities):
            if not px % 100:
                logging.info('    {}/{}'.format(px,len(possibilities)))
            term = 1
            for rowix,val in enumerate(p):
                k = 'x{}y{}'.format(colix,rowix)
                if not val:
                    term = And(term,Not(syms[k]))
                else:
                    term = And(term,syms[k])
            col_term = Or(col_term,term)
        col_terms.append(col_term)
    
    # Same for the rows

    logging.info('Building row terms')

    row_terms = []
    
    for rowix,row in enumerate(rows):
        logging.info('row {}/{}'.format(rowix+1,len(rows)))
        possibilities = generate_possibilities(row,len(cols))
        row_term = 0
        for px,p in enumerate(possibilities):
            if not px % 100:
                logging.info('    {}/{}'.format(px,len(possibilities)))
            term = 1
            for colix,val in enumerate(p):
                k = 'x{}y{}'.format(colix,rowix)
                if not val:
                    term = And(term,Not(syms[k]))
                else:
                    term = And(term,syms[k])
            row_term = Or(row_term,term)
        row_terms.append(row_term)

    # AND all column and row terms together
    # (i.e., for each column and row, at least one combination must be true)
    logging.info('Combining column and row terms.')
    all_terms = 1
    col_row_terms = col_terms+row_terms
    for termix,term in enumerate(col_row_terms):
        logging.info('    combined {}/{}'.format(termix,len(col_row_terms)))
        all_terms = And(all_terms,term)

    # Perform SAT and print result
    logging.info('Terms combined.')
    solution = sat(all_terms)
    print_solution(solution,len(cols),len(rows))



if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Slow nonogram solver using pyeda and SAT')
    parser.add_argument('NONOGRAM',help='The JSON file describing the nonogram')
    args = parser.parse_args()
    main(args.NONOGRAM)
    
