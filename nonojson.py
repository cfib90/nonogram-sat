import sys
import json

if __name__=="__main__":
    columns_read = False
    rows_read = False
    do_cols = False
    do_rows = False
    width = None
    height = None
    cols = []
    rows = []

    for _line in sys.stdin:
        line = _line.strip()
        if width is None and line.startswith('width'):
            _, width = line.split(' ')
            width = int(width)
        elif height is None and line.startswith('height'):
            _, height = line.split(' ')
            height = int(height)
        elif not columns_read and line == "columns":
            do_cols = True
        elif not rows_read and line == "rows":
            do_rows = True
        elif do_rows:
            if len(rows) == height:
                do_rows = False
                assert(line == '')
            else:
                rows.append(line)
        elif do_cols:
            if len(cols) == width:
                do_cols = False
                assert(line == '')
            else:
                cols.append(line)

    out = {'rows': [], 'cols': []}

    for col in cols:
        int_col = [int(c) for c in col.split(',')]
        out['cols'].append(int_col)
        
    for row in rows:
        int_row = [int(c) for c in row.split(',')]
        out['rows'].append(int_row)

    print(json.dumps(out,indent=4))
